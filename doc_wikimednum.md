**Documentation plateforme Mednum73**

__Objectifs__

 Reprendre ce qui est déjà écrit dans le readme sur le dépôt framagit du projet.
Disposer d'une plateforme ressources pour les différents acteurs engagés dans l'inclusion numérique, avec comme principales fonctions attendues
    - annuaire / cartographie des acteurs
    - qualification de l'offre (que proposent les acteurs)
    - ressources méthodos et expériences pour monter des projets de médiation numérique
    - possibilité de contribution facilitée pour les partenaires et acteurs du territoire


__Description des choix qui ont été faits__

Techniques

Le choix a été fait de se porter sur (Yeswiki)[lien], un "CMS - content system manager" basé sur Php/mySql qui permet de l'installer sur n'importe quel hébergement web. 
A noter qu'il est possible de disposer de son propre Wiki sans avoir à l'héberger, en le créant sur ferme.yeswiki.net (attention - toutes les fonctionnalités, notamment de personnalisation graphique, ne sont pas disponible si l'on crée son wiki sur ferme.yeswiki.net)

Au délà de la simplicité, Yeswiki permet de répondre aux objectifs identifiés plus haut - notamment la possibilité de gérer simplement une base de données et de faciliter la contribution par des formulaires.

Dans le cas de la plateforme Mednum73, nous avons fait le choix d'héberger le site sur un serveur mutualisé chez Gandi.net durant la pahse de développement. Une fois la plateforme mise en production, la question de l'hébergement pourra se poser.

!! A noter qu'il ne faut pas négliger l'aspect hébergement/nom de domaine lors de la réalisation d'une plateforme de ce type - notamment dans le cadre d'un portage en partenariat par une ou plusieurs institutions et la réalisation par un acteur tiers, il en va de l'administration et de la pérennité de la plateforme à moyen et long terme. 


Origine des données

Repndre le laius du dépôt framagit, avec les sources des données.

Méthode

**Générer la carte des lieux** 

Une fois effectué le recensement des différentes sources de données, nous avons créé un fichier CSV avec l'ensemble des lieux identifiés (en partant du fichier le plus important en volume - celui de Netpublic, et en ajoutant au fur et à mesure les données des autres fichiers).
Ensuite, afin de pouvoir en faire une cartographie, nous avons géocodé (c'est-à-dire déterminer des coordonnées géographique (x,y) à partir de l'adresse) le fichier grâce à l'outil d'(ETALAB)[adresse.data.gouv.fr/csv].
Nous avons ensuite fait un premier test de visualisation de la carte avec le service en ligne (Umap)[umap.openstreetmap.fr], disponible (ici)[carto.agate-territoires.fr/mednum/index.html]

**Intégrer la base de données des lieux dans la plateforme**

L'idée était ensuite d'intéger cette carte des lieux dans une plateforme répondant aux objectifs initiaux. 
Une fois le wiki installé sur notre hébergemment (tutoriel par ici)[yeswiki.net/documentation], nous avons créé deux briques pour l'annuaire carto des lieux de médiation numérique :

    - un formulaire bazar (lien doc)[lien vers doc bazar sur yeswiki] pour renseigner les lieux. Le choix des entrées/champs du formulaire a été fait afin de disposer d'un nombre d'informations réduites mais fondamentales (coordonnées de contact, modalités d'accueil, localisation) pour faciliter la mise à jour des fiches de chaque lieu.

    - un formulaire bazar afin d'intégrer le référentiel des services de la médiation numérique, qui permettra de qualifier l'offre des lieux et acteurs de la médiation numérique (description ci-dessous).

Une fois les formulaires créés, nous avons pu importer la base consolidée des lieux (en fichier csv) dans la base de donnée bazar du wiki. Pour se faire, nous avons utiliser la fonction __importer__ sur la page d'administration de bazar (icone roue crantée en haut à droite dans le menu du wiki - __gérer la base de données__).

!!Attention pour cette manipulation, à vérifier l'intitulée des colonnes qui doit correspondre aux noms des champs donnés lors de la création du formulaire - pour être sûr, utiliser la fonction __télécharger un exemple__ proposée sur la page __importer__ de la page administration de bazar.

**Intégrer le référentiel des services dans les fiches acteurs**

Suite à l'excellent travail menée par Garlann Nizon des Inforoutes de l'Ardeche sur le guide des services de la médiation numérique (disponible ici)[lien] nous avons pu récupérer le tableur contenant les données sources (c'est à dire un tableau avec une ligne par service).
Ensuite comme mentionné plus haut nous avons créer un formulaire bazar pouvant "accueillir" le référentiel,en réfléchissant à des listes permettant de catégoriser les services (comme il y en a en l'état 144, il fallait disposer de clé de tri). Nous avons donc créer une liste correspondant au "niveau", et une autre correspondant à la "thématique".
Nous avons ensuite importer ce fichier dans la base de données bazar (comme pour les fiches "lieux").

Une fois le référentiel intégré dans la base bazar, nous avons fait une passerelle, dans le formulaire "lieux", pour pouvoir associer a chaque fiche acteur les services rendus par ce dernier. Pour ce faire nous avons utiliser le champ __checkboxfiche__ en associant les fiches "référentiel". Nous avons également affiché le formulaire, sur la page de saisie, en plusieurs onglets afin de faciliter la lecture et le remplissage [lien]

**tout doux technique**

 - proposer un téléchargement des données csv de l'annuaire (bouton avec fonction export des listes acteurs)
 - reprendre les quelques détails génant liés à l'intégration des fiches référentiels ("text label" par exemple)
 - regarder pour modifier le css des bulles sur la carto, pour un affichage plus chiadé une fois l'offre de services renseignée
 - prototyper une interface de selection des acteurs par des parcours/besoins usagers (basé sur des filtres )

**tout doux organisationnelle**
- organiser les temps de présentation/contrib/animation de la plateforme dans les territoires avec les acteurs
- valider le portage de la plateforme par AGATE
